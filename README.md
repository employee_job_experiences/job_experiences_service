# JOB_EXPERIENCE_SERVICE

Ini merupakan aplikasi manajemen pengalaman kerja pegawai di suatu perusahaan atau instansi dimana data ini dapat digunakan untuk membantu mengolah data kepegawaian.

Untuk Job Experiences Service ini berisi aplikasi untuk management user login dan pendaftaran form pengalaman kerja. Di sini saya juga menyertakan file dump database mysql saya.

Untuk Dokumentasi API pada service ini yaitu :

# Manajemen User :

<details><summary>Click to expand</summary>

- **POST Create User** 

Digunakan untuk membuat user baru, adapun data role merupakan tipe data enum yang mempunyai value "admin" dan "user"

endpoint URL : http://127.0.0.1:5000/user/create

Body raw (json)

```
{
    "fullname":"<nama>",
    "email":"<email>",
    "password":"<password>",
    "username":"<username>",
    "role":"<admin | user>"
}
```

- **POST Login User** 

Digunakan untuk login user yang telah dibuat sebelumnya.

endpoint URL : http://127.0.0.1:5000/user/login

Body raw (json)

```
{
    "username":"<username>",
    "password":"<password>"
}
```

- **GET All Users** 

Untuk menampilkan semua list users yang terdaftar, pada endpoint ini hanya role admin yang hanya bisa melihat datanya.

endpoint URL : http://127.0.0.1:5000/users

**Authorization** : Bearer Token


- **GET All Users by Id** 

Untuk menampilkan list user berdasarkan id, pada endpoint ini hanya role admin yang bisa melihat semua datanya.

endpoint URL : http://127.0.0.1:5000/user/{id}

**Authorization** : Bearer Token


- **POST Update User** 

Untuk melakukan perubahan data user, data yang bisa diubah yaitu fullname dan email. Role admin bisa merubah semua data user, sedangkan role user hanya bisa mengubah data milik dia sendiri.

endpoint URL : http://127.0.0.1:5000/user/update/{id}

**Authorization** : Bearer Token

Body raw (json)

```
{
    "fullname":"<fullname>",
    "email":"<email>"
}
```

- **DEL Delete User**

Untuk menghapus data user. Role admin bisa menghapus semua data user, sedangkan role user hanya bisa menghapus data milik dia sendiri.

endpoint URL : http://127.0.0.1:5000/user/delete/{id}

**Authorization** : Bearer Token

</details>

# Manajemen Form Pengalaman Kerja :

<details><summary>Click to expand</summary>


- **POST Create Form Job Experiences**

Untuk menginputkan data pengalaman kerja, **form_user_id** diisikan dengan no id dari tabel form_user_register. Di tabel form_job_experiences terdapat kolom user_id, kolom ini akan otomatis terisi sesuai dengan id user waktu login user.

endpoint URL : http://127.0.0.1:5000/form_user/create

**Authorization** : Bearer Token

Body raw (json)
```
{
   {
    "form_user_id":<form_user_id>,
    "job_name":"<job_name>",
    "company":"<company>",
    "start_year":<year:YYYY>,
    "end_year":<year:YYYY>
}
}
```

- **POST Update Form Job Experiences** 

Untuk melakukan perubahan data pengalaman kerja user yang telah diinputkan berdasarkan user_id, data yang bisa diubah yaitu job_name, company, start_year dan end_year. Role admin bisa merubah semua data pengalaman kerja user, sedangkan role user hanya bisa mengubah data milik dia sendiri. Pada kolom body raw json ditambahkan **{id}** dari form_job_experiences yang akan diubah.

endpoint URL : http://127.0.0.1:5000/form_job/update/{user_id}

**Authorization** : Bearer Token

Body raw (json)

```
{
    "id" : <id>,
    "job_name":"<job_name>",
    "company":"<company>",
    "start_year":<year:YYYY>,
    "end_year":<year:YYYY>
    
}
```

- **DEL Delete Form Job Experiences**

Untuk menghapus data pengalaman kerja user. Role admin bisa menghapus semua data pengalaman kerja user, sedangkan role user hanya bisa menghapus data milik dia sendiri. Pada kolom body raw json ditambahkan **{id}** dari form_job_experiences yang akan dihapus.

endpoint URL : http://127.0.0.1:5000/form_job/delete/{user_id}

**Authorization** : Bearer Token

Body raw (json)

```
{
    "id":<id>
}
```



- **GET All Form Job Experiences Data** 

Untuk menampilkan semua data pengalaman kerja user yang sudah diinput, pada endpoint ini hanya role admin yang hanya bisa melihat datanya.

endpoint URL : http://127.0.0.1:5000/form_job

**Authorization** : Bearer Token



- **GET Form Job Experiences by User** 

Untuk menampilkan data pengalaman kerja yang merupakan hasil join data users, form_users_register dan form_job_experiences. Role admin bisa menampilkan semua data, sedangkan role user hanya menampilkan data milik dia sendiri.

endpoint URL : http://127.0.0.1:5000/form_job_combined

**Authorization** : Bearer Token

</details>

![ER Diagram](https://gitlab.com/employee_job_experiences/job_experiences_service/-/raw/main/ERD_Diagram.png)
