from app.models.usersModel import Users, Session, engine, Base
from werkzeug.security import generate_password_hash, check_password_hash
from flask import jsonify, request
from flask_jwt_extended import *
import os,json,datetime
from hashlib import pbkdf2_hmac

Base.metadata.create_all(engine)
session = Session()

# generate salt and hash for register and login users
def generate_salt():
    salt = os.urandom(16)
    return salt.hex()

def generate_hash(plain_pass, pass_salt):
    pass_hash = pbkdf2_hmac(
        "sha256",
        b"%b" % bytes(plain_pass, "utf-8"),
        b"%b" % bytes(pass_salt, "utf-8"),
        10000,
    ) 
    return pass_hash.hex()

#show users
@jwt_required()
def showUsers():
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    if (user_id[0]['role']=='admin'): 
        data = session.query(Users).all()
        try:
            data_dict['code'] = 200
            data_dict['success'] = True
            for row in data:
                col = ['id','fullname','email','password_hash','password_salt','username','role']
                val = row.id,row.fullname,row.email,row.password_hash,row.password_salt,row.username,row.role
                res = dict(zip(col,val))
                res_list.append(res)
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        except:
            data_dict['code'] = 401
            data_dict['success'] = False
            data_dict['message'] = "Data error"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 401
            return result
    else:
        data_dict['code'] = 403
        data_dict['success'] = False
        data_dict['message'] = "Permission not allowed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 403
        return result

#show user by id
@jwt_required()
def showUserbyId(id):
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    if (user_id[0]['role']=='admin'): 
        data = session.query(Users).filter(Users.id == '{}'.format(id))

        data_dict['code'] = 200
        data_dict['success'] = True
        for row in data:
            col = ['id','fullname','email','password_hash','password_salt','username','role']
            val = row.id,row.fullname,row.email,row.password_hash,row.password_salt,row.username,row.role
            res = dict(zip(col,val))
            res_list.append(res)
        if res_list:
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        else:
            data_dict['data'] = res_list
            data_dict['message'] = "Data not found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
    else:
        data_dict['code'] = 403
        data_dict['success'] = False
        data_dict['message'] = "Permission not allowed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 403
        return result

#register new user
def insertUser(**params):
    final_res = []
    data_dict = {}
    try:
        data_dict['code'] = 200
        data_dict['success'] = True
        pass_salt = generate_salt()
        pass_hash = generate_hash(params['password'],pass_salt)
        data = Users(params['fullname'],params['email'],pass_hash,pass_salt,params['username'],params['role'])
        session.add(data)
        session.commit()
        data_dict['message'] = "User created successfully"
        final_res.append(data_dict)
        return jsonify(final_res)
    except:
        data_dict['code'] = 400
        data_dict['success'] = False
        data_dict['message'] = "Create user failed"
        final_res.append(data_dict)
        return jsonify(final_res)

#login user
def userLogin(**params):
    
        cur_user = session.query(Users).filter(Users.username == '{}'.format(params['username']))
        res_list = []
        final_res = []
        data_dict = {}
        try: 
            for row in cur_user:
                col = ['id','fullname','email','password_hash','password_salt','username','role']
                val = row.id,row.fullname,row.email,row.password_hash,row.password_salt,row.username,row.role
                res = dict(zip(col,val))
                saved_pass_hash = row.password_hash
                saved_pass_salt = row.password_salt
                pass_hash = generate_hash(params['password'],saved_pass_salt)
                res_list.append(res)
            if pass_hash == saved_pass_hash:
                expires = datetime.timedelta(days=1)
                expires_refresh = datetime.timedelta(days=3)
                access_token = create_access_token(res_list, fresh=True, expires_delta=expires)
                data_dict['code'] = 200
                data_dict['success'] = True
                data_dict['data'] = res_list
                data_dict['message'] = "Data found"
                data_dict['token_access'] = access_token
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result
            else:
                data_dict['code'] = 401
                data_dict['success'] = False
                data_dict['message'] = "Your credential did not match"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 401
                return result
        except:
            data_dict['code'] = 401
            data_dict['success'] = False
            data_dict['message'] = "Username can't be found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 401
            return result  

#update only data fullname and email user by id
@jwt_required()
def updateUserById(id,**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    data = session.query(Users).filter(Users.id == '{}'.format(id)).first()
    try:
        if (user_id[0]['role']=='admin'):    
            if params['fullname'] and params['email']:
                data_dict['code'] = 200
                data_dict['success'] = True
                data.fullname = params['fullname']
                data.email = params['email']
                if data.fullname:
                    session.commit()
                    data_dict['message'] = "User updated successfully"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 200
                    return result
                else:
                    data_dict['code'] = 200
                    data_dict['success'] = True
                    data_dict['message'] = "User not found"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 200
                    return result

            else:
                data_dict['code'] = 400
                data_dict['success'] = False
                data_dict['message'] = "Fullname and email must be filled"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 400
            return result
        else:
            if id == user_id[0]['id']:
                if params['fullname'] and params['email']:
                    data_dict['code'] = 200
                    data_dict['success'] = True
                    data.fullname = params['fullname']
                    data.email = params['email']
                    if data.fullname:
                        session.commit()
                        data_dict['message'] = "User updated successfully"
                        final_res.append(data_dict)
                        result = jsonify(final_res)
                        result.status_code = 200
                        return result
                    else:
                        data_dict['code'] = 200
                        data_dict['success'] = True
                        data_dict['message'] = "User not found"
                        final_res.append(data_dict)
                        result = jsonify(final_res)
                        result.status_code = 200
                        return result
                else:
                    data_dict['code'] = 400
                    data_dict['success'] = False
                    data_dict['message'] = "Fullname and email must be filled"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 400
                    return result

            else:
                data_dict['code'] = 401
                data_dict['success'] = False
                data_dict['message'] = "You can only edit your own account"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 401
                return result
    except:
        data_dict['code'] = 401
        data_dict['success'] = False
        data_dict['message'] = "User can't be found"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 401
        return result  


#delete data user by id
@jwt_required()
def deleteUserById(id,**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    data = session.query(Users).filter(Users.id == '{}'.format(id)).one()
    if (user_id[0]['role']=='admin'):  
        session.delete(data)
        session.commit()
        data_dict['message'] = "User deleted successfully"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 200
        return result

    else:
        if id == user_id[0]['id']:
            session.delete(data)
            session.commit()
            data_dict['message'] = "User deleted successfully"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        else:
            data_dict['code'] = 401
            data_dict['success'] = False
            data_dict['message'] = "You can only delete your own account"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 401
            return result





   