from app import app
from app.controllers import form_jobController
from flask import Blueprint, request

form_job_blueprint = Blueprint("form_jobRouter",__name__)

@app.route("/form_job_combined/",methods=["GET"])
def showJoinJobForm():
    return form_jobController.showJoinJobForm()

@app.route("/form_job",methods=["GET"])
def showFormJob():
    return form_jobController.showFormJob()

@app.route("/form_job/create",methods=["POST"])
def insertFormJob():
    params = request.json
    return form_jobController.insertFormJob(**params)

@app.route("/form_job/update/<int:userid>",methods=["POST"])
def updateFormJobById(userid):
    params = request.json
    return form_jobController.updateFormJobById(userid,**params)

@app.route('/form_job/delete/<int:userid>',methods=['DELETE'])
def deleteFormJobById(userid):
    params = request.json
    return form_jobController.deleteFormJobById(userid,**params)

